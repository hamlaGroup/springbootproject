/**
 * 
 */
package com.chelabela.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

/**
 * @author hani
 *
 */
public class Main {
	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		Main thMain = new Main();
		FileInputStream serviceAccount = new FileInputStream(
				(thMain.getClass().getClassLoader().getResource("gcs.json").getFile()));
		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(serviceAccount))
				.setDatabaseUrl("https://chelabelaproject.firebaseio.com").build();
		FirebaseApp.initializeApp(options);
	
		Firestore db = FirestoreClient.getFirestore();
		db.getCollections().forEach((coll)->{
			ApiFuture<QuerySnapshot> query = coll.get();
			// ...
			// query.get() blocks on response
			QuerySnapshot querySnapshot = null;
			try {
				querySnapshot = query.get();
				List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
				for (QueryDocumentSnapshot document : documents) {
				  System.out.println("name: " + document.getString("name"));	 
				  System.out.println("email: " + document.getString("email"));
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		});
		DocumentReference docRef = db.collection("users").document("aturing");
		// Add document data with an additional field ("middle")
		Map<String, Object> data = new HashMap<>();
		data.put("first", "Alan");
		data.put("middle", "Mathison");
		data.put("last", "Turing");
		data.put("born", 1912);

		ApiFuture<WriteResult> result = docRef.set(data);
		System.out.println("Update time : " + result.get().getUpdateTime());

	}
}
