package com.chelabela.domain;

public class UserInformation {
	private String idUser;
	private String name;
	private String email;

	public UserInformation() {
	}

	public UserInformation(String idUser, String name, String email) {
		this.idUser = idUser;
		this.name = name;
		this.email = email;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
