package com.chelabela.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Configuration
public class FirebaseConfig {

	@Value("${firebase.path}")
	private String chatPath;
	// GOOGLE_APPLICATION_CREDENTIALS
	@Value(value = "classpath:google-services.json")
	private Resource gservicesConfig;

	@Value(value = "classpath:gcs.json")
	private Resource gcsConfig;

	@Value("${firebase.database-url}")
	private String databaseUrl;

	@Bean
	public FirebaseApp provideFirebaseOptions() throws IOException {
		FileInputStream serviceAccount = new FileInputStream((this.getClass().getClassLoader().getResource("gcs.json").getFile()));
		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(serviceAccount)).setDatabaseUrl("https://chelabelaproject.firebaseio.com").build();
		return FirebaseApp.initializeApp(options);
	}

	@Bean
	@Qualifier("main")
	public DatabaseReference provideDatabaseReference(FirebaseApp firebaseApp) {
	
		FirebaseDatabase.getInstance(firebaseApp).setPersistenceEnabled(false);
		return FirebaseDatabase.getInstance(firebaseApp).getReference();
	}

}
