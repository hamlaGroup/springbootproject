package com.chelabela.service;

public interface FirebaseService {

    /**
     * Open connection with Firebase and start listening for events
     */
    public void startFirebaseListener();

}
